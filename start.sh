#!/bin/sh

py3="/usr/bin/env python3"

tmux new-session -s "tele01-fidas" \; \
    split-window -h -p 50 \; \
    select-pane -t 0 \; \
    send-keys "$py3 main.py -b 127.0.0.15 authoritative -z zones/root.zone.json -l" C-m \; \
    split-window -v -p 85 \; \
    send-keys "$py3 main.py -b 127.0.0.20 authoritative -z zones/telematik.zone.json -l" C-m \; \
    split-window -v -p 83 \; \
    send-keys "$py3 main.py -b 127.0.0.30 authoritative -z zones/switch.telematik.zone.json -l" C-m \; \
    split-window -v -p 80 \; \
    send-keys "$py3 main.py -b 127.0.0.40 authoritative -z zones/router.telematik.zone.json -l" C-m \; \
    split-window -v -p 75 \; \
    send-keys "$py3 main.py -b 127.0.0.50 authoritative -z zones/fuberlin.zone.json -l" C-m \; \
    split-window -v -p 67 \; \
    send-keys "$py3 main.py -b 127.0.0.60 authoritative -z zones/homework.fuberlin.zone.json -l" C-m \; \
    split-window -v -p 50 \; \
    send-keys "$py3 main.py -b 127.0.0.70 authoritative -z zones/pcpools.fuberlin.zone.json -l" C-m \; \
    select-pane -t 7 \; \
    send-keys "$py3 main.py -b 127.0.0.10 recursive -z zones/root-servers.json" C-m \; \
    split-window -v -p 85 \; \
    send-keys "$py3 -m http.server -d www -b 127.0.0.31 8000" C-m \; \
    split-window -v -p 83 \; \
    send-keys "$py3 proxy.py" C-m \; \
    split-window -v -p 80 \; \
    send-keys 'sleep 3s' C-m \; \
    send-keys "$py3 main.py -b 127.0.0.3 stub -t a -q pcpools.fuberlin -H 127.0.0.10" C-m \; \
