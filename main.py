import argparse
import json
import socket
import time
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    subparsers.required = True
    parser.add_argument("-b", "--bind",
        action="store",
        default="127.0.0.1",
        dest="bind")
    parser.add_argument("-p", "--port",
        action="store",
        default="53053",
        type=int,
        dest="port")
    parser.add_argument("-d", "--delay",
        action="store",
        default="100",
        type=int,
        help="simulated network delay in ms",
        dest="delay")

    # stub
    parser_stub = subparsers.add_parser('stub')
    parser_stub.set_defaults(func=stub)
    parser_stub.add_argument("-q", "--query",
        action="store",
        required=True,
        dest="query")
    parser_stub.add_argument("-t", "--type",
        action="store",
        default="a",
        dest="query_type")
    parser_stub.add_argument("-r", "--dont-recurse",
        action="store_false",
        dest="recurse")
    parser_stub.add_argument("-H", "--ns-host",
        action="store",
        required=True,
        dest="ns_host")
    parser_stub.add_argument("-P", "--ns-port",
        action="store",
        default=53053,
        type=int,
        dest="ns_port")

    # recursive
    parser_rec = subparsers.add_parser('recursive')
    parser_rec.set_defaults(func=recursive)
    parser_rec.add_argument("-z", "--root-servers",
        action="store",
        required=True,
        dest="root_file")
    parser_rec.add_argument("-l", "--log",
        action="store_true",
        dest="log")
    parser_rec.add_argument("-c", "--disable-cache",
        action="store_false",
        dest="cache")

    # authoritative
    parser_auth = subparsers.add_parser('authoritative')
    parser_auth.set_defaults(func=authoritative)
    parser_auth.add_argument("-z", "--zone-file",
        action="store",
        required=True,
        dest="zone_file")
    parser_auth.add_argument("-l", "--log",
        action="store_true",
        dest="log")

    args = parser.parse_args()
    print(args)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        args.func(args, s)


def stub(args, sock):
    log, _ = get_logger()

    print(f"Started in stub-resolver mode at {args.bind}:{args.port}")
    start_time = time.time()

    msg = new_query(
        name=args.query,
        q_type=args.query_type,
        recurse=args.recurse
    )
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock_rcv:
        sock_rcv.bind((args.bind, args.port))
        send(sock, (args.ns_host, args.ns_port), msg, args.delay, log)
        data, addr = sock.recvfrom(1024)
        print(pretty_for_stub(json.loads(data.lower()), addr, start_time))


def recursive(args, sock):
    if args.log:
        log, f = get_logger(ip=args.bind, file=f"logs/{args.bind}.log")
    else:
        log, f = get_logger()
    counters = {
        "req_snd": 0,
        "req_rcv": 0,
        "resp_snd": 0,
        "resp_rcv": 0,
    }
    make_logstring(counters, log)

    cache = Cache(args.cache)

    print(f"Started in recursive resolver mode at {args.bind}:{args.port}")
    sock.bind((args.bind, args.port))

    root_servers = json.loads(Path(args.root_file).read_text())

    while True:
        data, client_addr = sock.recvfrom(1024)
        counters["req_rcv"] += 1
        make_logstring(counters, log)

        msg = json.loads(data.lower())
        msg["dns"]["flags"]["response"] = True
        msg["dns"]["qry"]["name"] = fix_name(msg["dns"]["qry"]["name"])
        name = msg["dns"]["qry"]["name"]

        msg_cache = cache.query(msg)
        if msg_cache is None:
            if name == ".":
                msg["dns"]["data"] = [root_servers[0]["addr"]]
                msg["dns"]["resp"]["ttl"] = [0]
                msg["dns"]["resp"]["name"] = [name]
                msg["dns"]["resp"]["type"] = ["ns"]
                msg["dns"]["flags"]["rcode"] = 0
            else:
                # start iterating
                ns_addr = (root_servers[0]["addr"], root_servers[0]["port"])
                i = 0
                while True:
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock_send:
                        print(f"asking {ns_addr} for {name}")

                        # begin lookup # (this really should be a function but it doesnt
                        # work for some reason)
                        msg_tmp = new_query(
                            name=name,
                            q_type=msg["dns"]["qry"]["type"],
                            recurse=False
                        )
                        msg_cache = cache.query(msg_tmp)
                        if msg_cache is not None:
                            msg_it = msg_cache
                        else:
                            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock_rcv:
                                sock_rcv.bind((args.bind, args.port+1))
                                send(sock, ns_addr, msg_tmp, args.delay, log)
                                counters["req_snd"] += 1
                                make_logstring(counters, log)

                                data, addr = sock.recvfrom(1024)
                                counters["resp_rcv"] += 1
                                make_logstring(counters, log)
                            msg_it = json.loads(data.lower())
                            if msg_it["dns"]["count"]["answers"] > 0:
                                cache.push(
                                    name=msg_it["dns"]["resp"]["name"][0],
                                    q_type=msg_it["dns"]["resp"]["type"][0],
                                    data=msg_it["dns"]["data"][0],
                                    ttl_rel=msg_it["dns"]["resp"]["ttl"][0]
                                )
                        # end lookup #

                    if i == 0 and not msg["dns"]["flags"]["recdesired"]:
                        msg = msg_it
                        break
                    elif (msg_it["dns"]["flags"]["rcode"] == 0 and
                        msg_it["dns"]["count"]["answers"] > 0):

                        if (msg_it["dns"]["resp"]["type"][0] == msg["dns"]["qry"]["type"] and
                            msg_it["dns"]["resp"]["name"][0] == name):
                            msg = msg_it
                            break
                        else:
                            ns_addr = (msg_it["dns"]["data"][0], 53053)
                    elif msg_it["dns"]["flags"]["rcode"] != 0:
                        msg = msg_it
                        break
                    i += 1
                # end iterating

        msg["dns"]["flags"]["authoritative"] = False
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock_send:
            send(sock_send, client_addr, msg, args.delay, log)
            counters["resp_snd"] += 1
            make_logstring(counters, log)


def authoritative(args, sock):
    if args.log:
        log, f = log, f = get_logger(ip=args.bind, file=f"logs/{args.bind}.log")
    else:
        log, f = get_logger()
    counters = {
        "req_snd": 0,
        "req_rcv": 0,
        "resp_snd": 0,
        "resp_rcv": 0,
    }
    make_logstring(counters, log)

    print(f"Started in authoritative server mode at {args.bind}:{args.port}")
    sock.bind((args.bind, args.port))

    zone = load_zone(args.zone_file)

    while True:
        data, addr = sock.recvfrom(1024)
        counters["req_rcv"] += 1
        make_logstring(counters, log)

        msg = json.loads(data.lower())
        msg["dns"]["flags"]["response"] = True
        msg["dns"]["qry"]["name"] = fix_name(msg["dns"]["qry"]["name"])

        found_records = [rr for rr in zone if (
            rr["type"] == msg["dns"]["qry"]["type"] and
            rr["name"] == msg["dns"]["qry"]["name"])]
        if len(found_records) < 1:
            split_name = msg["dns"]["qry"]["name"].split(".")
            for i in range(len(split_name)):
                found_records = [rr for rr in zone if (
                    rr["type"] == "ns" and
                    rr["name"] == fix_name(".".join(split_name[i:])))]
                if len(found_records) > 0:
                    break

        if len(found_records) < 1:
            msg["dns"]["flags"]["rcode"] = 3
        else:
            msg["dns"]["flags"]["rcode"] = 0
            msg["dns"]["flags"]["authoritative"] = True
            msg["dns"]["count"]["answers"] = len(found_records)
            msg["dns"]["resp"]["ttl"] = [rr["ttl"] for rr in found_records]
            msg["dns"]["resp"]["name"] = [rr["name"] for rr in found_records]
            msg["dns"]["resp"]["type"] = [rr["type"] for rr in found_records]

            msg["dns"]["data"] = [rr["data"] for rr in found_records]

        send(sock, addr, msg, args.delay, log)
        counters["resp_snd"] += 1
        make_logstring(counters, log)


def new_message() -> dict:
    return {
        "dns": {
            "qry": {
                "name": None, # (string) the queried name (e.g. google.com)
                "type": None, # (string) query type (e.g. A or MX)
            },
            "flags": {
                "response": None, # (boolean) whether message is a response
                "recdesired": None, # (boolean) whether recursion is desired
                "authoritative": None, # (boolean) whether the answer is authoritative
                "rcode": None, # (int) reply code
                    # 0               No error condition
                    # 1               Format error
                    # 2               Server failure
                    # 3               Name Error / not found (only auth)
                    # 4               Not Implemented
                    # 5               Refused
                    # 6-15            Reserved for future use.
            },
            "count": {
                "answers": None, # (int) number of RRs in answer section
            },
            # I don't really see why we have both a and ns here. I will just use
            # "data" instead and define the types via resp.type
            #"a": None, # ([string]) response to an A-type query (e.g. 172.217.16.206)
            #"ns": None, # ([string]) response to an NS-type query
            "data": None, # ([string]) response data fields
            "resp": {
                "ttl": None, # ([int]) ttl to ns or a response
                "name": None, # ([str]) actual names to the response
                "type": None, # ([str]) actual types to the response
            },
        },
    }


def new_query(name: str, q_type: str, recurse: bool) -> dict:
    msg = new_message()
    msg["dns"]["flags"]["response"] = False
    msg["dns"]["flags"]["recdesired"] = recurse
    msg["dns"]["qry"]["name"] = name
    msg["dns"]["qry"]["type"] = q_type.lower()

    return msg


def serialize(msg) -> str:
    return json.dumps(msg, separators=(",", ":"))


def send(sock, dest_addr: tuple, msg: dict, delay: int, log):
    if msg["dns"]["data"] is not None:
        msg["dns"]["count"]["answers"] = len(msg["dns"]["data"])
    else:
        msg["dns"]["count"]["answers"] = 0
    msg_b = serialize(msg).lower().encode(encoding="utf-8")
    time.sleep(delay / 1000)
    print(f"sending to {dest_addr}")
    sock.sendto(msg_b, dest_addr)


def load_zone(path) -> dict:
    if isinstance(path, str):
        path = Path(path)
    return json.loads(path.read_text().lower())


def fix_name(name: str) -> str:
    """adds the root "." to a domain name, if neccessary"""
    if len(name) == 0:
        name = "."
    elif name[-1] != ".":
        name += "."
    return name


def pretty_for_stub(msg, from_addr: str, start_time) -> str:
    """Beautifies a response for the stub resolver"""
    ret = (
        'Flags:\n'
        f'\tResponse:\t{msg["dns"]["flags"]["response"]}\n'
        f'\tRecDesired:\t{msg["dns"]["flags"]["recdesired"]}\n'
        f'\tAuthoritative:\t{msg["dns"]["flags"]["authoritative"]}\n'
        f'\tReply Code:\t{msg["dns"]["flags"]["rcode"]}\n'
        'Query:\n'
        f'\tType:\t{msg["dns"]["qry"]["type"].upper()}\n'
        f'\tName:\t{msg["dns"]["qry"]["name"]}\n'
    )
    if msg["dns"]["flags"]["rcode"] == 0:
        ret += f'Answer from {from_addr}:\n'
        for i in range(msg["dns"]["count"]["answers"]):
            ret += (
                '\t- '
                f'{msg["dns"]["resp"]["name"][i]}'
                f'\t{msg["dns"]["resp"]["ttl"][i]}'
                f'\t{msg["dns"]["resp"]["type"][i].upper()}'
                f'\t{msg["dns"]["data"][i]}\n'
            )
    ret += f'Took {(time.time() - start_time):.3f} seconds'

    return ret


def get_logger(ip=None, file=None):
    """returns a logger function

    Args:
        file: if provided, also log to this file
    """
    if file is None:
        return lambda x: print(int(time.time()), *x, sep="|"), None
    else:
        f = open(Path(file), "a")
        def logger(text: tuple):
            print(int(time.time()), *text, sep="|")
            print(int(time.time()), *text, sep="|", file=f, flush=True)
        return logger, f


def make_logstring(counters, log):
    log((
        counters["req_snd"],
        counters["req_rcv"],
        counters["resp_snd"],
        counters["resp_rcv"]
    ))


# idk, doesnt work for some reason
# def lookup(sock_send, name: str, bind_addr: tuple, ns_addr: tuple, q_type: str, delay: int) -> dict:
#     msg = new_query(
#         name=name,
#         q_type=q_type,
#         recurse=False
#     )
#     with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock_rcv:
#         sock_rcv.bind(bind_addr)
#         send(sock_send, ns_addr, msg, delay)
#         data, addr = sock_rcv.recvfrom(1024)
#         return json.loads(data.lower())


class Cache:
    def __init__(self, cache=True):
        """Init cache
        Args:
            cache: if false, don't actually cache anything.
        """
        self.cache = {}
        self.do_cache = cache
        # {
        #     ("name", "type"): {
        #         "ttl": 0,
        #         "data": "0.0.0.0"
        #     }
        # }


    def push(self, name: str, q_type: str, data: str, ttl_rel: int):
        if self.do_cache:
            print("pushing")
            self.cache.update({
                (name, q_type): {
                    "ttl": ttl_rel + int(time.time()),
                    "data": data
                    }
            })
        else:
            print("not pushing")


    def refresh(self):
        if self.do_cache:
            new = {}
            for k,v in self.cache.items():
                if v["ttl"] - 5 > int(time.time()):
                    new[k] = v
            self.cache = new


    def find(self, name: str, q_type: str):
        """returns data and relative ttl, or None, if miss"""
        self.refresh()
        name = fix_name(name)
        if (name,q_type) in self.cache:
            return (
                self.cache[(name,q_type)]["data"],
                self.cache[(name,q_type)]["ttl"] - int(time.time())
            )
        else:
            return None, None


    def query(self, msg: dict):
        """Same as find() but uses query messages. Returns None if miss, ready response msg if hit"""
        name = fix_name(msg["dns"]["qry"]["name"])
        q_type = msg["dns"]["qry"]["type"]

        cache_data, cache_ttl = self.find(name, q_type)
        if cache_data is not None:
            msg["dns"]["data"] = [cache_data]
            msg["dns"]["resp"]["ttl"] = [cache_ttl]
            msg["dns"]["resp"]["name"] = [name]
            msg["dns"]["resp"]["type"] = [q_type]
            msg["dns"]["flags"]["rcode"] = 0
            return msg
        else:
            return None


if __name__ == "__main__":
    main()
