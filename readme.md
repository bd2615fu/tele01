# Telematik Project 1

This repository solution to contains a solution to this years telematics project.
The exact requirements and scope for this project can be found in [this pdf](doc/project01.pdf).

## Usage

The project is written entirely in Python without any external libraries.

The simplest way to get started is with the provided [`start.sh`](start.sh) script (requires `tmux`):
```
$ ./start.sh
```
This opens a new `tmux`-window with several panes for all servers.
Note that your window size must be large enough to allow 7 stacked tmux panels and 2 side-by-side.
A maximized terminal window with a reasonable font size at 1080p seems to be sufficient.

![screenshot of tmux window](doc/start.png)*This is the expected output*

In the bottom-left pane one example command will be executed, then you can enter your own commands to query the DNS-servers.
You can see global options with:
```
$ python3 main.py --help
```
Command-specific options are shown with:
```
$ python3 main.py {stub, authoritative, recursive} --help
```
The query the recursive resolver at `127.0.0.10` for the A-record for `easy.homework.fuberlin`, you can enter:
```
$ python3 main.py --bind 127.0.0.3 stub --type a --query easy.homework.fuberlin --ns-host 127.0.0.10
```

A rough overview of the network's topology can be found [here](docs/overwiew.txt).

Additionally, there is an `http`-proxy and -server started to give a proof of concept of some more functionality.
By default, the server is at `127.0.0.31:8000` and the proxy is at `127.0.0.5:9000`.
To fetch a file from the server one can enter (requires curl):
```
$ curl http://127.0.0.31:8000/file   # directly
$ curl http://127.0.0.5:9000/http://switch.telematik:8000/file   # via the proxy and domain name
```

You can quit the `tmux`-session by pressing first `Ctrl+B`, then `:`, then typing `kill-session`, and then pressing `Enter`.
