import http.server
import json
import socket
import socketserver
import urllib.request

BIND = "127.0.0.5"
PORT = 9000
RESOLVER = "127.0.0.10"
RESOLVER_PORT = 53053
PROXIED = "switch.telematik"


def main():
    httpd = socketserver.ForkingTCPServer((BIND, PORT), Proxy)
    print (f"Now serving at {BIND}:{PORT}")
    httpd.serve_forever()


def lookup(name: str):
    msg = {
        "dns": {
            "qry": {
                "name": name,
                "type": "a"
            },
            "flags": {
                "response": False,
                "recdesired": True,
                "authoritative": None,
                "rcode": None
            },
            "count": {
                "answers": 0
            },
            "data": None,
            "resp": {
                "ttl": None,
                "name": None,
                "type":None
            }
        }
    }
    msg_b = json.dumps(msg, separators=(",", ":")).lower().encode(encoding="utf-8")

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.bind((BIND, PORT+1))
        s.sendto(msg_b, (RESOLVER, RESOLVER_PORT))
        data, _ = s.recvfrom(1024)

    data = json.loads(data)
    try:
        return data["dns"]["data"][0]
    except:
        return None


class Proxy(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        url=self.path[1:].lower()

        split_url = url.split("/")
        host_part = split_url[2].split(":")
        if host_part[0] == PROXIED.lower():
            new_host = lookup(host_part[0])
            if new_host is not None:
                host_part[0] = new_host
                split_url[2] = ":".join(host_part)
                url = "/".join(split_url)

        self.send_response(200)
        self.end_headers()
        self.copyfile(urllib.request.urlopen(url), self.wfile)


if __name__ == "__main__":
    main()
